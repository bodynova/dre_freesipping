<?php
$sMetadataVersion = '2.0';

$aModule          = [
    'id'          => 'dre_freeshipping',
    'title'       => '<img src="../modules/bender/dre_freeshipping/out/img/favicon.ico" height="20px" title="Bender Free Shipping ab">ody Versand frei ab...',
    'description' => 'displays configurable "free shipping for orders over ...€" notice in basket and minibasket',
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0.0',
    'author'      => 'Bodynova GmbH',
    'email'       => 'support@bodynova.de',
    'url'         => 'https://bodynova.de',
    'extend'      => [
        \OxidEsales\Eshop\Core\ViewConfig::class => \Bender\dre_freeshipping\Core\oxviewconfigdrefreeshipping::class
    ],
    'templates'   => [],
    'blocks'      => [
        [
            'template' => 'widget/minibasket/minibasket.tpl',
            'block'    => 'widget_minibasket_total',
            'file'     => '/Application/Views/Blocks/widget_minibasket_total.tpl'
        ],
        [
            'template' => 'page/checkout/basket.tpl',
            'block'    => 'basket_btn_next_bottom',
            'file'     => '/Application/Views/Blocks/basket_btn_next_bottom.tpl'
        ],
        [
            'template' => 'layout/topRow.tpl',
            'block'    => 'dre_freeShippingTopRowLeft',
            'file'     => '/Application/Views/Blocks/dre_TopRow_Left.tpl'
        ],
        [
            'template' => 'layout/topRow.tpl',
            'block'    => 'dre_freeShippingTopRowMiddle',
            'file'     => '/Application/Views/Blocks/dre_TopRow_Middle.tpl'
        ],
        [
            'template' => 'layout/topRow.tpl',
            'block'    => 'dre_freeShippingTopRowRight',
            'file'     => '/Application/Views/Blocks/dre_TopRow_Right.tpl'
        ],
    ],
    'settings'    => [
        [
            'group'    => 'dreFreeshippingMain',
            'name'     => 'iDreFreeShippingOver',
            'type'     => 'str',
            'value'    => '80',
            'position' => 0
        ],
        [
            'group'    => 'dreFreeshippingMain',
            'name'     => 'iDreFreeShippingTreshold',
            'type'     => 'str',
            'value'    => '50',
            'position' => 1
        ]
    ]
];
