<?php

/**
 *    bla-freeshipping
 *  Copyright (C) 2017  bestlife AG
 *  info:  oxid@bestlife.ag
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

$aLang = [
    'charset'                      => 'UTF-8',
    'FREE_SHIPPING_OVER'           => 'Nous prenons en charge les frais d\'envoi à partir d\'un montant de %s !',
    'ORDER_MORE_FOR_FREE_SHIPPING' => 'Commandez seulement <b>%s</b> en plus pour bénéficier de l\'envoi gratuit.',
    'YOU_SAVE_SHIPPING_COSTS'      => 'Vous économisez ainsi  <b>%s</b> de frais d\'envoi.',
    'ONLY_LEFT_FOR_FREE_SHIPPING'  => 'Pour une livraison gratuite, il faut ajouter encore ',
    'LOOK_AT_OUR_SALES'            => 'Découvrez nos autres articles en promotion à combiner',
    'OUR_SALES'                    => 'Nos promotions',
    'MST_MYSAVING_PERCENTSAVED'    => 'Vous économisez %s&#37;!',
];
