<?php

$style = '<style type="text/css">.groupExp a.rc b {font-size: medium; color: #ff3600; }.groupExp dt input.txt { width: 430px !important} .groupExp dl { display: block !important; } input.confinput {position: fixed; top: 20px; right: 70px; background: #008B2D; padding: 10px 25px; color: white; border: 1px solid black; cursor:pointer; font-size: 125%; } input.confinput:hover {outline: 3px solid #ff3600;} .groupExp dt textarea.txtfield {width: 430px; height: 150px;}</style>';
$aLang = [
    'charset'                               => 'UTF-8',
    'SHOP_MODULE_GROUP_dreFreeshippingMain' => 'Einstellungen',
    'SHOP_MODULE_iDreFreeShippingOver'      => 'Versandkosten frei ab ...EUR',
    'SHOP_MODULE_iDreFreeShippingTreshold'  => 'Der Hinweis wird erst ab diesem Warenkorb-Wert angezeigt',
    'SHOP_MODULE_iDreFreeShippingTelefone'  => 'Telefonnummer',
    'SHOP_MODULE_iDreFreeShippingOpenEar'   => 'Öffnungszeiten',

];
