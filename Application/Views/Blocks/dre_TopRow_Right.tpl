[{*oxifcontent ident="troheadercenter" object="_cont"}]
	<div id="headercenter[{$oView->getActiveLangAbbr()}]">
		[{*if $oxcmp_user->oxuser__oxpassword->value}]
				                    [{oxmultilang ident="GREETING"}]
				                    [{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
				                    <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account"}]">
				                        [{if $fullname}]
				                            [{$fullname}]
				                        [{else}]
				                            [{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
				                        [{/if}]
				                    </a>
				    [{/if}]
		[{$_cont->oxcontents__oxcontent->value}]
	</div>
[{/oxifcontent*}]
[{*include file="layout/right-buttons.tpl"*}]