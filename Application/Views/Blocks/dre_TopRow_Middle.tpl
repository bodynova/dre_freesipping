[{*$smarty.block.parent*}]
[{*if $oxcmp_basket->getProductsCount() && $oxcmp_basket->getDeliveryCosts() && $oxcmp_basket->getBruttoSum() > $oViewConf->freeShippingTreshold() && $oxcmp_basket->getBruttoSum() < $oViewConf->freeShippingOver()}]
	[{*strip}]
	[{math equation="x-y" x=$oViewConf->freeShippingOver() y=$oxcmp_basket->getBruttoSum() assign="ordermore" format="%.2f"}]
	[{assign var="Restkaufwert" value=$ordermore|number_format:2:",":' '|cat:$currency->sign}]
		<div class="text-center" style="display: table-cell; vert-align: middle">
			<a href="[{oxgetseourl ident='oxdeliveryinfo' type='oxcontent'}]" class="btn btn-warning" type="button" rel="nofollow">
				[{oxmultilang ident="ONLY_LEFT_FOR_FREE_SHIPPING"}] <span class="badge">[{$Restkaufwert}]</span>
			</a>
			<a href="[{oxgetseourl ident='oxdeliveryinfo' type='oxcontent'}]" class="btn btn-warning" type="button" rel="nofollow">
				[{oxmultilang ident="LOOK_AT_OUR_SALES"}] <span class="badge">Angebote</span>
			</a>
		</div>
	[{/strip*}]
[{*/if*}]

<div id="headercenterde">
	[{oxifcontent ident="dreheadercenter" object="_cont"}]
		<div class="col-lg-12 text-center" style="padding-bottom: 2px">
			[{* Begrüßung *}]
			[{*if $oxcmp_user->oxuser__oxpassword->value}]
				[{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
				[{oxmultilang ident="GREETING"}]
				<a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account"}]" style="color:white;">
					[{if $fullname}]
						[{$fullname}]
					[{else}]
						[{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
					[{/if}]
				</a>
			[{/if*}]
			[{$_cont->oxcontents__oxcontent->value}]
		</div>
	[{/oxifcontent}]
</div>