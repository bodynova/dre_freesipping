# dre_freesipping

Anzeige wieviel fehlt, bis zum kostenlosen Versand

### Composer Module installieren:
composer configurieren:
```php
composer config repositories.bender/dre_freeshipping git git@bitbucket.org:bodynova/dre_freesipping.git
```
das id_rsa.pub auf dem Server auslesen:
```php
cat ~/.ssh/id_rsa.pub
```
anschließend das repository installieren:
```php
composer require bender/dre_freeshipping
```
