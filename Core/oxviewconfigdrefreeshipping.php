<?php

namespace Bender\dre_freeshipping\Core;
/**
 *    dre-freeshipping
 **/

class oxviewconfigdrefreeshipping extends oxviewconfigdrefreeshipping_parent
{
    public function freeShippingOver()
    {
        return (int) \OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam( "iDreFreeShippingOver" );
    }
    public function freeShippingTreshold()
    {
        return (int) \OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam( "iDreFreeShippingTreshold" );
    }

}

